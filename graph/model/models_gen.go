// Code generated by github.com/99designs/gqlgen, DO NOT EDIT.

package model

type AggregatedProject struct {
	CommaSeparatedNames string `json:"commaSeparatedNames"`
	AggregatedCount     int    `json:"aggregatedCount"`
}

type Project struct {
	Name        string `json:"name"`
	Description string `json:"description"`
	ForksCount  int    `json:"forksCount"`
}
