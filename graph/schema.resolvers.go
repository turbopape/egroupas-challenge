package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"challenge/graph/generated"
	"challenge/graph/model"
	"challenge/pkg/repository"
	"context"
	"os"
)

func (r *queryResolver) Projects(ctx context.Context, n int) ([]*model.Project, error) {
	gitLabUrl := os.Getenv("GITLAB_URL")
	if gitLabUrl == "" {
		gitLabUrl = "https://gitlab.com/api/graphql"
	}
	gitlabPersonalToken := os.Getenv("GITLAB_PERSONAL_TOKEN")
	nodes, errNodes := repository.GetLastNProjectsFromGitlab(gitLabUrl, gitlabPersonalToken, n)
	if errNodes != nil {
		return nil, errNodes
	}
	projects := []*model.Project{}
	for _, node := range nodes {
		projects = append(projects, &model.Project{
			Name:        node.Name,
			Description: node.Description,
			ForksCount:  int(node.ForksCount),
		})
	}
	return projects, nil
}

func (r *queryResolver) AggregateProjects(ctx context.Context, n int) (*model.AggregatedProject, error) {
	gitLabUrl := os.Getenv("GITLAB_URL")
	if gitLabUrl == "" {
		gitLabUrl = "https://gitlab.com/api/graphql"
	}
	gitlabPersonalToken := os.Getenv("GITLAB_PERSONAL_TOKEN")
	nodes, errNodes := repository.GetLastNProjectsFromGitlab(gitLabUrl, gitlabPersonalToken, n)
	if errNodes != nil {
		return nil, errNodes
	}
	aggrCount, projectNames := repository.GetAggregatedValuesFromNode(nodes)
	return &model.AggregatedProject{
		CommaSeparatedNames: projectNames,
		AggregatedCount:     aggrCount,
	}, nil
}

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

type queryResolver struct{ *Resolver }
