# eGRoup AS Gitlab Forks Challenge
## The skeleton
This work revolves around the stock [99design's gql](github.com/99designs/gqlgen ) graphQl generation
for the server stack. It uses raw graphql through REST approach to query gitlab.
## Working with the repo
You install dependencies etc than spin off the server
```shell
cd <the repo>
go run challenge
```
You need to have the following env vars set:
```shell
GITLAB_PERSONAL_TOKEN: your personal access token
GITLAB_URL : defaults to "https://gitlab.com/api/graphql"
GITLAB_MAX_PROJECTS: if 0, will be used to restrict the number of projects. defaults to 5
```
To help testing, we use an .env file at the root of the project(on my local machine, ignored in git):
```shell
GITLAB_PERSONAL_TOKEN=my-personal-token
GITLAB_URL="https://gitlab.com/api/graphql"
GITLAB_MAX_PROJECTS=5
```
## Testing
```shell
cd <your project root>/pkg/{repository,service}
go test
```
testing the service spins off a graphql server in the background, makes the query and tests against direct fetching
from gitlab (using our repository layer).

## Queries
We implemented two graphql Queries. 
 - The query as per the challenge:
```graphql
{
    aggregateProjects(n: 5) {
        commaSeparatedNames
        aggregatedCount
    }
}
```
Gives the result:
```json
{
  "data": {
    "aggregateProjects": {
      "commaSeparatedNames": "hcs_utils,K,Heroes of Wesnoth,Leiningen,TearDownWalls",
      "aggregatedCount": 13
    }
  }
}
```

- And we also have one project listing query:
```graphql
{
    projects(n: 5) {
        name
        description
        forksCount
    }
}
```
which gives
```
{
  "data": {
    "projects": [
      {
        "name": "hcs_utils",
        "description": "",
        "forksCount": 1
      },
      {
        "name": "K",
        "description": "",
        "forksCount": 1
      },
      {
        "name": "Heroes of Wesnoth",
        "description": "",
        "forksCount": 5
      },
      {
        "name": "Leiningen",
        "description": "",
        "forksCount": 1
      },
      {
        "name": "TearDownWalls",
        "description": "",
        "forksCount": 5
      }
    ]
  }
}
```


