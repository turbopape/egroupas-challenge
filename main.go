package main

import "challenge/pkg/service"

//go:generate go run github.com/99designs/gqlgen generate
func main() {
	service.Run()
}
