package service_test

import (
	"bytes"
	"challenge/pkg/repository"
	"challenge/pkg/service"
	"encoding/json"
	"fmt"
	"github.com/buger/jsonparser"
	"github.com/joho/godotenv"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"testing"
	"time"
)

func TestServiceLayer(t *testing.T) {
	err := godotenv.Load("../../.env")
	if err != nil {
		fmt.Println("Error loading .env file")
	}

	gitLabUrl := os.Getenv("GITLAB_URL")
	gitlabPersonalToken := os.Getenv("GITLAB_PERSONAL_TOKEN")
	if gitlabPersonalToken == "" {
		t.Errorf("could not get Gitlab Personal Token to call the grapqhl API")
		return
	}
	go func() {
		// Running the service
		log.Printf("running the service for testing...")
		service.Run()
	}()
	time.Sleep(time.Second)
	aggregratedCountFromService, commaSeparatedNamesFromService, errAggrService := GetAggregatedCountAndNamesFromServiceLayer(5)
	if errAggrService != nil {
		t.Errorf("could not get aggregated count from service layer %s", errAggrService)
		return
	}

	nodes, errNodes := repository.GetLastNProjectsFromGitlab(gitLabUrl, gitlabPersonalToken, 5)
	if errNodes != nil {
		t.Errorf("could not get aggregated count from gitlab's graphql %s", errNodes)
		return
	}

	aggrCountFromGitlab, commaSeparatedNamesFromGitlab := repository.GetAggregatedValuesFromNode(nodes)

	if aggregratedCountFromService != aggrCountFromGitlab {
		t.Errorf("aggragted fork count mismatch, from Service: %d, from Gitlab: %d",
			aggregratedCountFromService,
			aggrCountFromGitlab)
	}

	if commaSeparatedNamesFromService != commaSeparatedNamesFromGitlab {
		t.Errorf("comma separated names mismatch, from Service: %s, from Gitlab: %s",
			commaSeparatedNamesFromService,
			commaSeparatedNamesFromGitlab)
	}
}

func GetAggregatedCountAndNamesFromServiceLayer(n int) (int, string, error) {

	serviceUrl := "http://localhost:8080/query"

	gqlQuery := fmt.Sprintf(`{
					aggregateProjects(n: %d) {
						commaSeparatedNames
						aggregatedCount
					}
				}`, n)

	queryData := map[string]string{"query": gqlQuery}
	queryJsonData, err := json.Marshal(queryData)

	log.Printf("--- GetLastNProjectsFromGitlab: Getting %d projects with query: %s", n, gqlQuery)
	req, err := http.NewRequest(http.MethodPost, serviceUrl, bytes.NewBuffer(queryJsonData))
	if err != nil {
		return 0, "", fmt.Errorf("--- GetLastNProjectsFromGitlab: could not create http request %s", err)

	}
	httpclient := &http.Client{
		Timeout: time.Second * 10,
	}

	req.Header.Add("Content-Type", "application/json")

	response, errResponse := httpclient.Do(req)
	if errResponse != nil {
		return 0, "", errResponse

	}
	defer response.Body.Close()
	if response.StatusCode != http.StatusOK {
		log.Printf("--- GetLastNProjectsFromGitlab: could not get project nodes from gitlab with status: %s",
			response.Status)
		return 0, "", fmt.Errorf("could not get proejct nodes with status: %s", response.Status)

	}

	responseBody, errResponseBody := ioutil.ReadAll(response.Body)
	if errResponseBody != nil {
		log.Printf("--- GetLastNProjectsFromGitlab: could not Read response body %s", errResponseBody)
		return 0, "", errResponseBody
	}

	aggrForksCounts, errAggrForksCounts := jsonparser.GetInt(responseBody, "data", "aggregateProjects", "aggregatedCount")
	if errAggrForksCounts != nil {
		return 0, "", errAggrForksCounts
	}

	commaSeparatedNames, errCommaSeparatedNames := jsonparser.GetString(responseBody, "data", "aggregateProjects", "commaSeparatedNames")
	if errCommaSeparatedNames != nil {
		return 0, "", errCommaSeparatedNames
	}

	return int(aggrForksCounts), commaSeparatedNames, nil

}
