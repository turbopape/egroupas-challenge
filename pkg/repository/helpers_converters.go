package repository

func GetAggregatedValuesFromNode(nodes []*Node) (int, string) {
	aggrCount := 0
	projectNames := ""
	for nodeIdx, node := range nodes {
		aggrCount += int(node.ForksCount)
		projectNames += node.Name
		if nodeIdx < len(nodes)-1 {
			projectNames += ","
		}
	}
	return aggrCount, projectNames
}
