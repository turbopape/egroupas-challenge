package repository

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/buger/jsonparser"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"
)

const (
	DefaultMaxProjects = 5
)

type Node struct {
	Name        string `json:"name"`
	Description string `json:"description"`
	ForksCount  int32  `json:"forksCount"`
}

// GetLastNProjectsFromGitlab Gets last N projects from Gitlab using GitlabUrl and gitlabToken
func GetLastNProjectsFromGitlab(gitLabUrl, gitlabToken string, n int) ([]*Node, error) {

	// making sure n holds something significant
	if n == 0 {
		newN, errConvNewN := strconv.Atoi(os.Getenv("GITLAB_MAX_PROJECTS"))
		if errConvNewN != nil {
			newN = DefaultMaxProjects
		}
		n = newN
	}

	// preparing the query
	gqlQuery := fmt.Sprintf(`query last_projects($n: Int = %d) {
				  projects(last:$n) {
					nodes {
					  name
					  description
					  forksCount
					}
				  }
				}`, n)

	//Calling the gitlab GQL Endpoint
	httpclient := &http.Client{
		Timeout: time.Second * 10,
	}
	queryData := map[string]string{"query": gqlQuery}
	queryJsonData, err := json.Marshal(queryData)

	log.Printf("--- GetLastNProjectsFromGitlab: Getting %d projects with query: %s", n, gqlQuery)
	req, err := http.NewRequest(http.MethodPost, gitLabUrl, bytes.NewBuffer(queryJsonData))
	if err != nil {
		log.Printf("--- GetLastNProjectsFromGitlab: could not create http request %s", err)
		return nil, err
	}

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", gitlabToken))

	response, errResponse := httpclient.Do(req)
	if errResponse != nil {
		return nil, errResponse
	}
	defer response.Body.Close()
	if response.StatusCode != http.StatusOK {
		log.Printf("--- GetLastNProjectsFromGitlab: could not get project nodes from gitlab with status: %s",
			response.Status)
		return nil, fmt.Errorf("could not get proejct nodes with status: %s", response.Status)
	}

	responseBody, errResponseBody := ioutil.ReadAll(response.Body)
	if errResponseBody != nil {
		log.Printf("--- GetLastNProjectsFromGitlab: could not Read response body %s", errResponseBody)
		return nil, errResponseBody
	}

	// Parsing the gitlab response body into an array of Nodes
	nodes := []*Node{}
	_, errParseNodes := jsonparser.ArrayEach(responseBody,
		func(value []byte, dataType jsonparser.ValueType, offset int, errToBeReturned error) {
			// using jsonparser, I unmarshal result data element by element into the result nodes array
			var node Node
			errParseNode := json.Unmarshal(value, &node)
			if errParseNode != nil {
				errToBeReturned = errParseNode
				return
			}
			nodes = append(nodes, &node)
		}, "data", "projects", "nodes")

	if errParseNodes != nil {
		log.Printf("--- GetLastNProjectsFromGitlab: could not parse project node %s", errParseNodes)
		return nil, errParseNodes
	}
	log.Printf("--- GetLastNProjectsFromGitlab: Successfully got project nodes from Gitlab")
	return nodes, nil
}
