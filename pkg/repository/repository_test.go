package repository_test

import (
	"challenge/pkg/repository"
	"fmt"
	"github.com/joho/godotenv"
	"os"
	"testing"
)

func TestGetLastNProjectsFromGitlab(t *testing.T) {
	err := godotenv.Load("../../.env")
	if err != nil {
		fmt.Println("Error loading .env file")
	}
	gitLabUrl := os.Getenv("GITLAB_URL")
	gitlabPersonalToken := os.Getenv("GITLAB_PERSONAL_TOKEN")
	if gitlabPersonalToken == "" {
		t.Errorf("could not get Gitlab Personal Token to call the grapqhl API")
		return
	}
	nodes, errNodes := repository.GetLastNProjectsFromGitlab(gitLabUrl, gitlabPersonalToken, 5)
	if errNodes != nil {
		t.Errorf("could not get project data nodes %s", errNodes)
		return
	}
	if len(nodes) != 5 {
		t.Errorf("expected 5 nodes got %d", len(nodes))
		return
	}

}
